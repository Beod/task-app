<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get(	'/', array('as' => 'getCalender', 'uses' => 'App\Http\Controllers\TaskController@get_calender') );

Route::post( 'addTask/', array('before'=>'csrf', 'as' => 'addTask', 'uses' => 'App\Http\Controllers\TaskController@add_task') );

Route::post( 'removeTask/', array('before'=>'csrf', 'as' => 'removeTask', 'uses' => 'App\Http\Controllers\TaskController@remove_task') );

Route::get( 'showDay/', array('before'=>'csrf', 'as' => 'showDay', 'uses' => 'App\Http\Controllers\TaskController@show_day') );