<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Task;
use Exception;

class TaskController extends Controller
{
    // private const HOURSOFTHEDAY = ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00', '22:00', '23:00'];

    public function add_task(Request $request)
    {  
        //validate received data incase html/js is tampered with.
        $request->validate(
            [
                'name' => 'required|max:100',
                'description' => 'required|max:255',
                'date' => 'required|date_format:Y-m-d|max:10',
                'start_time' => 'required|max:8',
                'end_time' => 'required|max:8',
            ]
        );
        //format times to HH:MM e.g. 13:00.
        $request->merge( 
            [ 
                'start_time' => date( "H:i", strtotime( $request->input('start_time') ) ),
                'end_time' => date( "H:i", strtotime( $request->input('end_time') ) ),
            ] 
        );
        $task = Task::create($request->all());

        return redirect()->action([TaskController::class, 'show_day'], ['success' => 'true', 'message' => 'Task made for ' . $task->name . " at " . $task->date . "!", 'date' => $task->date]);
    }

    public function remove_task(Request $request){
       $task = Task::findorfail( $request->input('id') );
       $task->delete();
        return redirect()->action([TaskController::class, 'show_day'], ['success' => 'true', 'message' => 'Task delete for ' . $task->name . " at " . $task->date . "!", 'date' => $task->date]);
    }

    public function get_calender(Request $request){
        $task_dates = [];
        foreach(Task::all() as $task){ array_push($task_dates,$task->date); }     
        return view('homepage', ['task_dates'=>$task_dates]);
    }

    public function show_day(Request $request){
        $day_details = [];
        foreach( Task::where('date','=',$request->input('date'))->get() as $task){
           array_push( $day_details, [$task->id, $task->name, $task->description, $task->date, date_format($task->start_time,'H:i'), date_format($task->end_time,'H:i')] );
        }
        #structure of day_details array:
        #   $day_details = [
        #       [task->id, task->name, task->description, task->date, task->start_time, task->end_time],
        #       [task->id, task->name, task->description, task->date, task->start_time, task->end_time],
        #       [task->id, task->name, task->description, task->date, task->start_time, task->end_time],
        #       ...
        #   ]
        $context = ['success'=>'false', 'day_details'=>$day_details, 'date'=>$request->input('date')];
        if ( $request->input('success') == 'true' ) {
            $context['success'] = $request->input('success');
            $context['message'] = $request->input('message');
        }
        return view('showday', $context);
    }

}
