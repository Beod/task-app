<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['name','description','date','start_time','end_time'];
    protected $casts = ['start_time' => 'datetime','end_time' => 'datetime', 'date','datetime'];
}
