@extends('layouts.base')

@section('styles')
<style type="text/css">
	.task-background{background-color: rgba(247, 183, 218, 0.5); }
</style>
@endsection

@section('content')
<div id="app">
   @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div>
	    <div class="ui raised very text blue segment">
	    	<div class="ui center aligned header">
		    	@if($day_details)
				    All Tasks On {{$date}}
				@else
				    There are no tasks for this date.
			    @endif
		    </div>
	    </div>
	</div>

	<div class="ui placeholder segment mb-4">
	  <div class="ui two column stackable center aligned grid">
	    <div class="ui vertical divider"><i class="arrow alternate circle right icon"></i></div>
	    <div class="middle aligned row">
	      <div class="column">
		    <div class="ui segments container mt-3" style="width:300px;">
				<div class="">
					@if($date >= date('Y-m-d'))
						<div class="ui center aligned header mt-2"><label>Add A Task To This Date</label></div>
						<hr>
				        <form @submit="loading" method="post" id="add_task_form" action="{{ route('addTask') }}">
				            @csrf
				            <label for="name">Name:</label>
				            <input type="text" name="name" class="form-control" maxlength="100" required>
				            <label for="description">Description:</label>
				            <input type="text" name="description" class="form-control" maxlength="255" required>
				            <select name="date" required hidden>
				            	<option selected value="{{$date}}"></option>
				            </select>
				            <label for="start_time">Start Time:</label>
				            <select name="start_time" id="start_time" @change="onChange" v-model="start_time" class="form-control" required><option selected value="">--Select a start time--</option></select>
				            <label for="end_time">End Time:</label>
				            <select name="end_time" id="end_time" class="form-control" required disabled><option selected value="">--Select a start time first--</option></select>
				            <!-- <button class="btn btn-warning mt-2"  id="add_task_button">Add Task</button> -->
				            <button id="add_task_button" type="submit" class="ui icon button bg-warning mt-2 mb-2">Add Task<i class="add icon"></i></button>
				        </form>
				    @else
					    Tasks cannot be added to past dates.
			        @endif
			    </div>
			</div>
	      </div>
	      <div class="column">
			    <div class="ml-3" style="height: 500px; width:100%;">
					<vue-cal selected-date="{{$date}}" :events="events" :disable-views="['years','year','month','week']" class="vuecal--blue-theme" :time-from="0 * 60" :time-to="24 * 60" :time-step="60" />
				</div>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- Modal -->
	<div class="ui modal">
	  <i class="close icon"></i>
	  <div class="header">
	    Task Details
	  </div>
	  <div class="image content">
	    <div class="ui medium image">
	      <img src="">
	    </div>
	    <div class="description">
	      <div class="ui header">We've auto-chosen a profile image for you.</div>
	      <p>We've grabbed the following image from the <a href="" target="_blank">gravatar</a> image associated with your registered e-mail address.</p>
	      <p>Is it okay to use this photo?</p>
	    </div>
	  </div>
	  <div class="actions">
	    <div class="ui black deny button">
	      Nope
	    </div>
	    <div class="ui positive right labeled icon button">
	      Yep, that's me
	      <i class="checkmark icon"></i>
	    </div>
	  </div>
	</div>

</div>
@endsection

@section('scripts')
	<script type="text/javascript">
	//Populate start time select widget and remove taken slots.
	var all_hrs_of_the_day = `
		@foreach ( range(0,23) as $val )
        	@if($val > 9)
            	<option value="{{$val}}:00">{{$val}}:00</option>
        	@else
            	<option value="0{{$val}}:00">0{{$val}}:00</option>
        	@endif	
		@endforeach
	`
	$('#start_time').append(all_hrs_of_the_day)
	// create array with all taken time slots as tasks = [{start_time : '00:00', end_time : '06:00'}, {start_time : '07:00', end_time : '11:00'}, ...]
	var tasks = [
		@foreach($day_details as $task)
			@if( $loop->last )
				{start_time : '{{$task[4]}}', end_time : '{{$task[5]}}'}
			@else
				{start_time : '{{$task[4]}}', end_time : '{{$task[5]}}'},
			@endif 
		@endforeach
	]
	//Convert time from HH:mm to integer and use as index to remove taken time slots.
	tasks.forEach((task)=>{
		let start_time;
		let end_time;
		if(task.start_time.slice(0,1).includes('0')){start_time = parseInt(task.start_time.slice(1,2))}
  		else{start_time = parseInt(task.start_time.slice(0,2))}
  		if(task.end_time.slice(0,1).includes('0')){end_time = parseInt(task.end_time.slice(1,2))}
  		else{end_time = parseInt(task.end_time.slice(0,2))}
  		for (var i = start_time; i <= end_time; i++) {
  			let hr_to_remove;
  			if (i < 10) { hr_to_remove = "0" + String(i) + ":00" } 
  			else{ hr_to_remove = String(i) + ":00" }
  				// alert(hr_to_remove)
  			$('option:contains('+ hr_to_remove +')').remove()
  		}
	});

	// Gen calender for the year.
	new Vue({
	  components: { 'vue-cal': vuecal },
	  el: '#app',
	  data: () => ({
	  	start_time:'',
	    events: [
	    //$task array description:
	    //	task[1] task[3] = date , task[4] = start_time and task[5] = end_time
		 @foreach($day_details as $task)
		      {
		        start: '{{$task[3]}} {{$task[4]}}',
		        end: '{{$task[3]}} {{$task[5]}}',
		        title: '{{$task[1]}}',
		        content: '<i class="v-icon calendar icon"></i><br>{{$task[2]}} ',
		        class: 'task-background',
		        
		      },
		 @endforeach
	    ]
	  }),
	  methods: {
	  	onChange: function (){
	  		$('#start_time').children('option:contains(--Select a start time--)').remove()
	  		let start_time; 
	  		if(this.start_time.slice(0,1).includes('0')){start_time = parseInt(this.start_time.slice(1,2))}
	  		else{start_time = parseInt(this.start_time.slice(0,2))}
  			let remaining_hrs = []
  			for (var i = start_time + 1; i < 24; i++) {
  				remaining_hrs.push(i)
  			}
  			$('#end_time').html('')
  			let end_times = [@foreach($day_details as $task)'{{$task[5]}}',@endforeach]
  			
  			for (hr of remaining_hrs){
  				if (hr < 10){hr = '0' + String(hr) + ':00'}
  				else{hr = String(hr) + ':00'}
  				if ( !end_times.includes(hr) ){
		  			$('#end_time').append(`<option value="${hr}">${hr}</option>`)
		  		}
  			}
  			let tasks = [@foreach($day_details as $task){start_time : '{{$task[4]}}', end_time : '{{$task[5]}}'},@endforeach]
  			tasks.forEach(
  				(task)=>{
						let start_time;
  						if(task.start_time.slice(0,1).includes('0')){start_time = parseInt(task.start_time.slice(1,2))}
				  		else{start_time = parseInt(task.start_time.slice(0,2))}
				  		let currently_selected_start_time = $('#start_time').val();

				  		if(currently_selected_start_time.slice(0,1).includes('0')){currently_selected_start_time = parseInt(currently_selected_start_time.slice(1,2))}
				  		else{currently_selected_start_time = parseInt(currently_selected_start_time.slice(0,2))}
						//If a selected start-time is before an already exisiting task then end-times for new task must be less than start time of already exisitng task.
				  		if (currently_selected_start_time < start_time ){
				  			$('#end_time').html('')
				  			$('#end_time').removeAttr('disabled')
				  			for (var i = currently_selected_start_time + 1; i < start_time; i++){
				  				if (i < 10){
						  			$('#end_time').append(`<option value="${'0'+String(i)+':00'}">  ${'0'+String(i)+':00'} </option>`)
						  		}
						  		else{
									$('#end_time').append(`<option value="${String(i)+':00'}"> ${String(i) + ':00'} </option>`)
						  		}
					  		}
				  		}
				  		else{
					  		let end_time;
					  		if(task.end_time.slice(0,1).includes('0')){end_time = parseInt(task.end_time.slice(1,2))}
					  		else{end_time = parseInt(task.end_time.slice(0,2))}
						
					  		for (var i = start_time; i <= end_time; i++) {
					  			let hr_to_remove;
					  			if (i < 10) { hr_to_remove = "0" + String(i) + ":00" } 
					  			else{ hr_to_remove = String(i) + ":00" }
					  			$('option:contains('+ hr_to_remove +')').remove()
					  		}
					  	}
  				}
  			)
  			
	  		$('#end_time').children('option:contains(--Select a start time first--)').remove()
  			$('#end_time').removeAttr('disabled')
	  	},
	  	loading: (e)=>{
	  		$('#add_task_button').html(`Loading...<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>`)
	  	},
	  },
	})

	$('.vuecal__event-content').click(()=>{$('.ui.modal').modal('show')})

    </script>
@endsection