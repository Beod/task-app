<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
	        <meta charset="utf-8">
	        <meta name="viewport" content="width=device-width, initial-scale=1">

	        <title>To Do App</title>
	        <!-- Add a favicon -->
	        <link rel="shortcut icon" type="image/jpg" href="{{asset('images/checklist.jpg')}}" sizes="16x16w">

	        <!-- Google fonts -->
			<link rel="preconnect" href="https://fonts.gstatic.com">
			<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;700&display=swap" rel="stylesheet">
	        <style type="text/css">
	        	.oswald{font-family: 'Oswald', sans-serif;}
	        </style>

	        <!-- Boostrap css cdn -->
	        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha512-k78e1fbYs09TQTqG79SpJdV4yXq8dX6ocfP0bzQHReQSbEghnS6AQHE2BbZKns962YaqgQL16l7PkiiAHZYvXQ==" crossorigin="anonymous" /> -->

	        <!-- Semantic ui css -->
	        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.css" integrity="sha512-+AdxWMqfOdtwMuXK482e5OgHYzN06s97n3NTAeAdTlUvlIgCBx3SUKvMculJIedylDfVCaQFdD6HaojI4aeBUA==" crossorigin="anonymous" />

    	    <!-- JQuery js -->
			<script type="text/javascript" src="{{asset('js/jquery/jquery.min.js')}}"></script>

	        <!-- Boostrap js cdn -->
	    <!-- 	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
	    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf" crossorigin="anonymous"></script>

	    	<!-- Semantic ui js -->
	    	<script src="{{ asset('themes/Semantic-UI/semantic.js') }}"></script>
	    	
	        <!-- Link Vue Javascript -->
		    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

		    <!-- Link VCalendar Javascript (Plugin automatically installed) -->
		    <script src='https://unpkg.com/v-calendar'></script>

	        <!-- Vue-cal js -->
		    <script src="https://unpkg.com/vue-cal"></script>
		    <link href="https://unpkg.com/vue-cal/dist/vuecal.css" rel="stylesheet">

		    <style type="text/css"> .ui.modal{position: fixed; top: 50px; height: 350px;}</style>

		    @yield('styles')
    </head>
    <body class="oswald">
			<div class="ui inverted segment">
			  <div class="ui stackable inverted secondary menu">
			    <a href="{{ route('getCalender') }}" class="item">
			      <img src="{{asset('images/checklist.jpg')}}" style="height: 50px; width: 50px; border-radius: 50%;">
			    </a>
			    <a class="item">
			      Home
			    </a>
			  </div>
			</div>
			
	    	<div class="container-fluid">
		    	@yield('content')
		    </div>

	    @yield('scripts')
    </body>
</html>