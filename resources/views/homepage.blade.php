@extends('layouts.base')

	@section('content')
	    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="ui segments container">
	        <div class="ui segment"><h2 class="ui center aligned header">Your Calender For The Year</h2><h4 class="ui center aligned header">Click on a date to see or add tasks for that day. Highlited dates have tasks.</h4></div>
		</div>
		<div class="container mt-4 mb-4" style="height: 200px;">
            <div class="col"></div>
            <div class="col-auto d-flex justify-content-center">
			<v-calendar :min-date="today" :max-date="end_of_year"  :columns="2" :rows="6" :attributes="attrs" id="calender" color="pink" /></div>
            <div class="col"></div>
		</div>
    @endsection

    @section('scripts')
        <!-- Initialize calender -->
	    <script>
    		new Vue({
		        el: '#calender',
		        data: {
		          selectedDate: null,
                  today: '{{date('Y-m-d')}}',
                  end_of_year: '2021-12-31',
		          attrs: [
			        {
			          key: '',
			          highlight: { color:'pink', fillMode: 'light', },
			          dates: @php echo json_encode($task_dates); @endphp,
			          dot:false,
			          bar:false,
			          content:'pink',
			        },

			      ],
		        }
    		}); 
	    </script>
        <!-- Show day when a date is clicked -->
        <script type="text/javascript">
            var show_day_url = '{{ route('showDay',['date'=>'']) }}'
            $('.vc-day-content.vc-focusable').click( 
                    function (){ 
                        var date = $(this).parent().attr('class').slice(10,20) 
                        location.replace(show_day_url + date) 
                    }
            );
        </script>
    @endsection
